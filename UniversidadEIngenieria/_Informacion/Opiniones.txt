Nico Pellegrinet
1) Estudiar ingenier�a en inform�tica pienso que tiene varios beneficios. En primer lugar, por el simple hecho de ser una ingenier�a brinda una amplia variedad de conocimientos que hacen a una gran formaci�n de la persona. En segundo lugar, permite incorporar conocimientos de las diferentes �reas de la inform�tica, pudiendo as� desempe�arse en diferentes �mbito laborales.

2) Los principales conceptos incorrectos que tiene la gente sobre la carrera son:

"Arreglan computadoras?"
"Ten�s que estudiar un mont�n de cosas de electr�nica"
"Es muy dif�cil"
"Hacen programitas con pantalla negra y letras blancas"


Franco Trucco
1) Uno de los beneficios m�s grandes, a mi parecer, es que es una carrera que te incita a estar actualizado en cuanto a novedades tecnol�gicas. Otra cosa, que pr�cticamente es por lo que eleg� la carrera yo, te mantiene pensando todo el tiempo ya que una gran parte de la carrera se trata sobre l�gica.
Otra cosa que se me ocurre que tiene como beneficio es el hecho de que es una carrera muy abarcativa.. ten�s much�simas especialidades para elegir
2) La gente es muy extremista.. Est�n los que piensan que aprendes a c�mo utilizar editores de texto, hojas de c�lculo, cosas as� de simples.. y est�n los que piensan que es una carrera super compleja que no muchos pueden hacer..
 Generalmente se nos piensa como "hackers" y que aprendemos cosas relacionadas a eso.


Ignacio Franco
1) Mucha salida laboral si te esforz�s en aprender y siempre est�s predispuesto a encarar cosas nuevas. Encontrarte con que la inform�tica es algo que va mucho m�s all� de lo que uno piensa y que est� en constante auge.
2) El primer concepto incorrecto es que sea una carrera muy dif�cil de recorrer, por el contrario, si le dedic�s lo que necesita en el d�a a d�a, normalmente vas a salir airoso. Otro concepto equivocado que recuerdo es que para estudiarla "hab�a que ser crack con los n�meros", si bien est� bueno entenderlo r�pido (por lo que v� de algunos compa�eros que les pasaba), a medida que avanza la carrera se te va abriendo la cabeza y empez�s a agarrar m�s r�pido ese tipo de conocimiento (yo siempre oscil� entre el 6 y el 8 en matem�tica y no tuve problemas para entender las cosas, si sirve como par�metro).

Agu Gauchat
1)
- Gran cantidad de demanda de mano de obra.
- Diversidad de �reas de aplicaci�n.

2)
- Que la salida laboral es netamente apuntada a la programaci�n (no es necesario programar si es que no te gusta, no es mi caso).
- Que es imposible llevar la carrera al d�a {hasta ahora} (cuesta un poco, pero se puede).

Nahue burdisso
1) Me parece que el beneficio mas grande que te brinda estudiar esta carrera es que si uno es apasionado de resolver problemas y entender como ciertas cosas funcionan, te permite sumergirte en un mundo repleto de desaf�os. Adem�s abarca tantos �mbitos y tiene tantas ramificaciones que es muy dificil no encontrar algo que te guste mucho y que no se relacione directamente con la carrera.

2) Conceptos err�neos no sabr�a muy bien que decirte, porque la gente tampoco sabe explicar muy bien en que consiste la carrera, pero lo que recuerdo que imaginaba era que mucho del trabajo diario en si, implicaba reparar hardware, y aunque es una posible salida laboral, el enfoque al desarrollo que se le da en esta facultad, te permite engancharte mucho con esa faceta y a mi me termino gustando mucho mas.
eeem si me ocurre algo mas te aviso

Mariano ferrero
1) Creo que tiene dos grandes beneficios, por un lado que es una carrera que tiene y seguramente tendr� tambi�n en el futuro una gran salida laboral. Por otro, creo que te da las herramientas b�sicas y el conocimiento necesario para poder desarrollar soluciones que le resuelvan - o al menos simplifiquen - la vida a las dem�s personas
2) No es un concepto que yo haya tenido, pero s� lo tienen muchas personas que desconocen: estudiar esta carrera no tiene como consecuencia directa el conocer c�mo arreglar PCs y ese tipo de cosas :); a lo que se dedica un profesional egresado de la carrera dista mucho de eso, aunque hay excepciones.